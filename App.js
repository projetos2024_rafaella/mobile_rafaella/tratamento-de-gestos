import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Home from "./src/home";
import ImageMoviment from './src/image';
import CountMoviment from './src/countMoviment';

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
      <Stack.Screen name="Home" component={Home}/>
        <Stack.Screen name="CountMoviment" component={CountMoviment} />
          <Stack.Screen name="ImageMoviment" component={ImageMoviment} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

