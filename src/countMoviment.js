import React , {useRe, useEffect, useState, useRef} from "react";
import { View, Dimensions, PanResponder, Button, Text } from "react-native";

const CountMoviment = () =>{
    const [count, setCount] = useState(0);
    const screenHeigth = Dimensions.get("window").height;
    const gestureThreshold = screenHeigth * 0.25;

    const panResponder = useRef(
        PanResponder.create({
            onStartShouldSetPanResponder:()=>true,
            onPanResponderMove:(event,gestureState)=>{},
            onPanResponderRelease:(event,gestureState)=>{
                if(gestureState.dy < -gestureThreshold){
                    setCount((prevCount) => prevCount +1);
                }
                
            },
        })
    ).current;

    return(<View
     {...panResponder.panHandlers}
        style={{ flex: 1, alignItens: "center", justifyContent: "center", left:"29%"}}
        >
        <Text> Valor do Contador: {count}</Text>
        </View>
        );
};
export default CountMoviment;