import React, { useRef, useState } from "react";
import { View, Dimensions, PanResponder, Image, StyleSheet } from "react-native";

const ImageMoviment = () => {
  const [count, setCount] = useState(0);
  const [currentImage, setCurrentImage] = useState(0);
  const screenWidth = Dimensions.get("window").width;
  const gestureThreshold = screenWidth * 0.5;

  const images = [
   require="https://uploads.maisgoias.com.br/2023/07/6b4efcac-barbie-rica1.jpg"
  ];

  const panResponder = useRef(
    PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderMove: (event, gestureState) => {},
      onPanResponderRelease: (event, gestureState) => { 
        if (gestureState.dx > gestureThreshold) {
          // Deslizar para direita
          const newCount = count + 1;
          setCount(newCount);
          setCurrentImage(newCount % images.length);
        } else if (gestureState.dx < -gestureThreshold) {
          // Deslizar para esquerda
          const newCount = count - 1;
          setCount(newCount >= 0 ? newCount : images.length - 1);
          setCurrentImage(newCount >= 0 ? newCount % images.length : images.length - 1);
        }
      },
    })
  ).current;

  return (
    <View {...panResponder.panHandlers} style={styles.container}>
      <Image source={images[currentImage]} style={styles.image} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  image: {
    width: 200,
    height: 200,
    resizeMode: "cover",
  },
});

export default ImageMoviment;
